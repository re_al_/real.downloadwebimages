﻿namespace ReAl.DownloadWebImages.App
{
    partial class TelegramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTelegramCodigo = new System.Windows.Forms.TextBox();
            this.btnTelegramAutorizar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtTelegramCodigo);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 58);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Código de activación";
            // 
            // txtTelegramCodigo
            // 
            this.txtTelegramCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelegramCodigo.Location = new System.Drawing.Point(6, 16);
            this.txtTelegramCodigo.Name = "txtTelegramCodigo";
            this.txtTelegramCodigo.Size = new System.Drawing.Size(228, 32);
            this.txtTelegramCodigo.TabIndex = 0;
            // 
            // btnTelegramAutorizar
            // 
            this.btnTelegramAutorizar.Location = new System.Drawing.Point(153, 76);
            this.btnTelegramAutorizar.Name = "btnTelegramAutorizar";
            this.btnTelegramAutorizar.Size = new System.Drawing.Size(99, 23);
            this.btnTelegramAutorizar.TabIndex = 1;
            this.btnTelegramAutorizar.Text = "Autorizar App";
            this.btnTelegramAutorizar.UseVisualStyleBackColor = true;
            this.btnTelegramAutorizar.Click += new System.EventHandler(this.btnTelegramAutorizar_Click);
            // 
            // TelegramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 106);
            this.Controls.Add(this.btnTelegramAutorizar);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TelegramForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Telegram";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtTelegramCodigo;
        private System.Windows.Forms.Button btnTelegramAutorizar;
    }
}