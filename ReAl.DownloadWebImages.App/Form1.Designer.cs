﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ReAl.DownloadWebImages.App
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtDwnUrl = new System.Windows.Forms.TextBox();
            this.btnDwnImagenes = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.prbDwnProgreso = new System.Windows.Forms.ProgressBar();
            this.lblAvance = new System.Windows.Forms.Label();
            this.btnImgIr = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtImgUrl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDwnNombreAlbum = new System.Windows.Forms.TextBox();
            this.chkIterar = new System.Windows.Forms.CheckBox();
            this.txtDwnIteracionesCant = new System.Windows.Forms.NumericUpDown();
            this.txtDwnIteracionesDesde = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage0 = new System.Windows.Forms.TabPage();
            this.btnConfApply = new System.Windows.Forms.Button();
            this.btnConfigLoad = new System.Windows.Forms.Button();
            this.btnConfigSave = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnTelegramAutorizar = new System.Windows.Forms.Button();
            this.txtTelegramNumero = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtTelegramApiHash = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTelegramApiId = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTumblrBlog = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTumblrTokenSecret = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTumblrToken = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTumblrConsumerSecret = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTumblrConsumerKey = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnImgurTokens = new System.Windows.Forms.Button();
            this.txtImgurRefreshToken = new System.Windows.Forms.TextBox();
            this.txtImgurAccessToken = new System.Windows.Forms.TextBox();
            this.txtMashApeKey = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtImgurAccountId = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtImgurAccountUsername = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtImgurClientId = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnDwnReddit = new System.Windows.Forms.Button();
            this.lstDwnArchivos = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label23 = new System.Windows.Forms.Label();
            this.txtImgDescripcion = new System.Windows.Forms.TextBox();
            this.lstImgArchivos = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.prbImgProgreso = new System.Windows.Forms.ProgressBar();
            this.btnImgSeleccionarCarpeta = new System.Windows.Forms.Button();
            this.txtImgCarpetaOrigen = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtImgNombreAlbum = new System.Windows.Forms.TextBox();
            this.btnImgSubirUser = new System.Windows.Forms.Button();
            this.btnImgSubir = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lstTmbArchivos = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.txtTmbCaption = new System.Windows.Forms.TextBox();
            this.prbTmbProgreso = new System.Windows.Forms.ProgressBar();
            this.btnTmbSeleccionarCarpeta = new System.Windows.Forms.Button();
            this.txtTmbCarpetaOrigen = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnTmbSubir = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lstTelArchivos = new System.Windows.Forms.ListView();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTelNroDestino = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTelCaption = new System.Windows.Forms.TextBox();
            this.btnTelEnviar = new System.Windows.Forms.Button();
            this.prbTelProgreso = new System.Windows.Forms.ProgressBar();
            this.btnTelSeleccionarCarpeta = new System.Windows.Forms.Button();
            this.txtTelCarpetaOrigen = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtRedditUsername = new System.Windows.Forms.TextBox();
            this.txtRedditPassword = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtDwnIteracionesCant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDwnIteracionesDesde)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPage0.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtDwnUrl
            // 
            this.txtDwnUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDwnUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDwnUrl.Location = new System.Drawing.Point(6, 19);
            this.txtDwnUrl.Name = "txtDwnUrl";
            this.txtDwnUrl.Size = new System.Drawing.Size(594, 29);
            this.txtDwnUrl.TabIndex = 1;
            // 
            // btnDwnImagenes
            // 
            this.btnDwnImagenes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDwnImagenes.Location = new System.Drawing.Point(437, 95);
            this.btnDwnImagenes.Name = "btnDwnImagenes";
            this.btnDwnImagenes.Size = new System.Drawing.Size(163, 33);
            this.btnDwnImagenes.TabIndex = 12;
            this.btnDwnImagenes.TabStop = false;
            this.btnDwnImagenes.Text = "Obtener Imagenes";
            this.btnDwnImagenes.UseVisualStyleBackColor = true;
            this.btnDwnImagenes.Click += new System.EventHandler(this.btnDwnImagenes_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "URL con las imagenes que se desean descargar:";
            // 
            // prbDwnProgreso
            // 
            this.prbDwnProgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prbDwnProgreso.Location = new System.Drawing.Point(6, 134);
            this.prbDwnProgreso.Name = "prbDwnProgreso";
            this.prbDwnProgreso.Size = new System.Drawing.Size(594, 30);
            this.prbDwnProgreso.TabIndex = 2;
            // 
            // lblAvance
            // 
            this.lblAvance.AutoSize = true;
            this.lblAvance.Location = new System.Drawing.Point(6, 51);
            this.lblAvance.Name = "lblAvance";
            this.lblAvance.Size = new System.Drawing.Size(82, 13);
            this.lblAvance.TabIndex = 3;
            this.lblAvance.Text = "Listos?? Fuera!!";
            // 
            // btnImgIr
            // 
            this.btnImgIr.Location = new System.Drawing.Point(266, 152);
            this.btnImgIr.Name = "btnImgIr";
            this.btnImgIr.Size = new System.Drawing.Size(44, 23);
            this.btnImgIr.TabIndex = 9;
            this.btnImgIr.TabStop = false;
            this.btnImgIr.Text = "Ir";
            this.btnImgIr.UseVisualStyleBackColor = true;
            this.btnImgIr.Click += new System.EventHandler(this.btnIr_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "URL Album:";
            // 
            // txtImgUrl
            // 
            this.txtImgUrl.BackColor = System.Drawing.Color.LightSalmon;
            this.txtImgUrl.Location = new System.Drawing.Point(6, 154);
            this.txtImgUrl.Name = "txtImgUrl";
            this.txtImgUrl.ReadOnly = true;
            this.txtImgUrl.Size = new System.Drawing.Size(254, 20);
            this.txtImgUrl.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Nombre Album:";
            // 
            // txtDwnNombreAlbum
            // 
            this.txtDwnNombreAlbum.Location = new System.Drawing.Point(110, 68);
            this.txtDwnNombreAlbum.Name = "txtDwnNombreAlbum";
            this.txtDwnNombreAlbum.Size = new System.Drawing.Size(304, 20);
            this.txtDwnNombreAlbum.TabIndex = 6;
            // 
            // chkIterar
            // 
            this.chkIterar.AutoSize = true;
            this.chkIterar.Location = new System.Drawing.Point(427, 50);
            this.chkIterar.Name = "chkIterar";
            this.chkIterar.Size = new System.Drawing.Size(109, 17);
            this.chkIterar.TabIndex = 4;
            this.chkIterar.Text = "Iterar paginas %%";
            this.chkIterar.UseVisualStyleBackColor = true;
            this.chkIterar.CheckedChanged += new System.EventHandler(this.chkIterar_CheckedChanged);
            // 
            // txtDwnIteracionesCant
            // 
            this.txtDwnIteracionesCant.Enabled = false;
            this.txtDwnIteracionesCant.Location = new System.Drawing.Point(548, 69);
            this.txtDwnIteracionesCant.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtDwnIteracionesCant.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtDwnIteracionesCant.Name = "txtDwnIteracionesCant";
            this.txtDwnIteracionesCant.Size = new System.Drawing.Size(42, 20);
            this.txtDwnIteracionesCant.TabIndex = 10;
            this.txtDwnIteracionesCant.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // txtDwnIteracionesDesde
            // 
            this.txtDwnIteracionesDesde.Enabled = false;
            this.txtDwnIteracionesDesde.Location = new System.Drawing.Point(473, 69);
            this.txtDwnIteracionesDesde.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtDwnIteracionesDesde.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtDwnIteracionesDesde.Name = "txtDwnIteracionesDesde";
            this.txtDwnIteracionesDesde.Size = new System.Drawing.Size(42, 20);
            this.txtDwnIteracionesDesde.TabIndex = 8;
            this.txtDwnIteracionesDesde.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(424, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Desde:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(523, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Al:";
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage0);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(614, 541);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage0
            // 
            this.tabPage0.Controls.Add(this.groupBox4);
            this.tabPage0.Controls.Add(this.btnConfApply);
            this.tabPage0.Controls.Add(this.btnConfigLoad);
            this.tabPage0.Controls.Add(this.btnConfigSave);
            this.tabPage0.Controls.Add(this.groupBox3);
            this.tabPage0.Controls.Add(this.groupBox2);
            this.tabPage0.Controls.Add(this.groupBox1);
            this.tabPage0.Location = new System.Drawing.Point(4, 22);
            this.tabPage0.Name = "tabPage0";
            this.tabPage0.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage0.Size = new System.Drawing.Size(606, 515);
            this.tabPage0.TabIndex = 4;
            this.tabPage0.Text = "Configuración";
            this.tabPage0.UseVisualStyleBackColor = true;
            // 
            // btnConfApply
            // 
            this.btnConfApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfApply.Location = new System.Drawing.Point(300, 476);
            this.btnConfApply.Name = "btnConfApply";
            this.btnConfApply.Size = new System.Drawing.Size(125, 33);
            this.btnConfApply.TabIndex = 14;
            this.btnConfApply.TabStop = false;
            this.btnConfApply.Text = "Aplicar Configuración";
            this.btnConfApply.UseVisualStyleBackColor = true;
            this.btnConfApply.Click += new System.EventHandler(this.btnConfApply_Click);
            // 
            // btnConfigLoad
            // 
            this.btnConfigLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigLoad.Location = new System.Drawing.Point(6, 476);
            this.btnConfigLoad.Name = "btnConfigLoad";
            this.btnConfigLoad.Size = new System.Drawing.Size(163, 33);
            this.btnConfigLoad.TabIndex = 13;
            this.btnConfigLoad.TabStop = false;
            this.btnConfigLoad.Text = "Cargar Configuración";
            this.btnConfigLoad.UseVisualStyleBackColor = true;
            this.btnConfigLoad.Click += new System.EventHandler(this.btnConfigLoad_Click);
            // 
            // btnConfigSave
            // 
            this.btnConfigSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigSave.Location = new System.Drawing.Point(431, 476);
            this.btnConfigSave.Name = "btnConfigSave";
            this.btnConfigSave.Size = new System.Drawing.Size(163, 33);
            this.btnConfigSave.TabIndex = 13;
            this.btnConfigSave.TabStop = false;
            this.btnConfigSave.Text = "Grabar Configuración";
            this.btnConfigSave.UseVisualStyleBackColor = true;
            this.btnConfigSave.Click += new System.EventHandler(this.btnConfigSave_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnTelegramAutorizar);
            this.groupBox3.Controls.Add(this.txtTelegramNumero);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.txtTelegramApiHash);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.txtTelegramApiId);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Location = new System.Drawing.Point(3, 321);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(594, 74);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Telegram";
            // 
            // btnTelegramAutorizar
            // 
            this.btnTelegramAutorizar.Location = new System.Drawing.Point(497, 17);
            this.btnTelegramAutorizar.Name = "btnTelegramAutorizar";
            this.btnTelegramAutorizar.Size = new System.Drawing.Size(91, 23);
            this.btnTelegramAutorizar.TabIndex = 6;
            this.btnTelegramAutorizar.Text = "Autorizar App";
            this.btnTelegramAutorizar.UseVisualStyleBackColor = true;
            this.btnTelegramAutorizar.Click += new System.EventHandler(this.btnTelegramAutorizar_Click);
            // 
            // txtTelegramNumero
            // 
            this.txtTelegramNumero.Location = new System.Drawing.Point(375, 19);
            this.txtTelegramNumero.Name = "txtTelegramNumero";
            this.txtTelegramNumero.Size = new System.Drawing.Size(116, 20);
            this.txtTelegramNumero.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(291, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Numero Origen";
            // 
            // txtTelegramApiHash
            // 
            this.txtTelegramApiHash.Location = new System.Drawing.Point(100, 45);
            this.txtTelegramApiHash.Name = "txtTelegramApiHash";
            this.txtTelegramApiHash.Size = new System.Drawing.Size(488, 20);
            this.txtTelegramApiHash.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 48);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Api Hash";
            // 
            // txtTelegramApiId
            // 
            this.txtTelegramApiId.Location = new System.Drawing.Point(100, 19);
            this.txtTelegramApiId.Name = "txtTelegramApiId";
            this.txtTelegramApiId.Size = new System.Drawing.Size(185, 20);
            this.txtTelegramApiId.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Api ID";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTumblrBlog);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtTumblrTokenSecret);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtTumblrToken);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtTumblrConsumerSecret);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtTumblrConsumerKey);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(6, 162);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(594, 153);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tumblr";
            // 
            // txtTumblrBlog
            // 
            this.txtTumblrBlog.Location = new System.Drawing.Point(100, 123);
            this.txtTumblrBlog.Name = "txtTumblrBlog";
            this.txtTumblrBlog.Size = new System.Drawing.Size(185, 20);
            this.txtTumblrBlog.TabIndex = 9;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 126);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Blog";
            // 
            // txtTumblrTokenSecret
            // 
            this.txtTumblrTokenSecret.Location = new System.Drawing.Point(100, 97);
            this.txtTumblrTokenSecret.Name = "txtTumblrTokenSecret";
            this.txtTumblrTokenSecret.Size = new System.Drawing.Size(488, 20);
            this.txtTumblrTokenSecret.TabIndex = 7;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 100);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Token Secret";
            // 
            // txtTumblrToken
            // 
            this.txtTumblrToken.Location = new System.Drawing.Point(100, 71);
            this.txtTumblrToken.Name = "txtTumblrToken";
            this.txtTumblrToken.Size = new System.Drawing.Size(488, 20);
            this.txtTumblrToken.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 74);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Token:";
            // 
            // txtTumblrConsumerSecret
            // 
            this.txtTumblrConsumerSecret.Location = new System.Drawing.Point(100, 45);
            this.txtTumblrConsumerSecret.Name = "txtTumblrConsumerSecret";
            this.txtTumblrConsumerSecret.Size = new System.Drawing.Size(488, 20);
            this.txtTumblrConsumerSecret.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Consumer Secret";
            // 
            // txtTumblrConsumerKey
            // 
            this.txtTumblrConsumerKey.Location = new System.Drawing.Point(100, 19);
            this.txtTumblrConsumerKey.Name = "txtTumblrConsumerKey";
            this.txtTumblrConsumerKey.Size = new System.Drawing.Size(488, 20);
            this.txtTumblrConsumerKey.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Consumer Key";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnImgurTokens);
            this.groupBox1.Controls.Add(this.txtImgurRefreshToken);
            this.groupBox1.Controls.Add(this.txtImgurAccessToken);
            this.groupBox1.Controls.Add(this.txtMashApeKey);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtImgurAccountId);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtImgurAccountUsername);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.txtImgurClientId);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(594, 150);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Imgur";
            // 
            // btnImgurTokens
            // 
            this.btnImgurTokens.Location = new System.Drawing.Point(291, 11);
            this.btnImgurTokens.Name = "btnImgurTokens";
            this.btnImgurTokens.Size = new System.Drawing.Size(145, 23);
            this.btnImgurTokens.TabIndex = 6;
            this.btnImgurTokens.Text = "Obtener Tokens Imgur";
            this.btnImgurTokens.UseVisualStyleBackColor = true;
            this.btnImgurTokens.Click += new System.EventHandler(this.btnImgurTokens_Click);
            // 
            // txtImgurRefreshToken
            // 
            this.txtImgurRefreshToken.Location = new System.Drawing.Point(100, 91);
            this.txtImgurRefreshToken.Name = "txtImgurRefreshToken";
            this.txtImgurRefreshToken.Size = new System.Drawing.Size(488, 20);
            this.txtImgurRefreshToken.TabIndex = 3;
            // 
            // txtImgurAccessToken
            // 
            this.txtImgurAccessToken.Location = new System.Drawing.Point(100, 65);
            this.txtImgurAccessToken.Name = "txtImgurAccessToken";
            this.txtImgurAccessToken.Size = new System.Drawing.Size(488, 20);
            this.txtImgurAccessToken.TabIndex = 3;
            // 
            // txtMashApeKey
            // 
            this.txtMashApeKey.Location = new System.Drawing.Point(100, 39);
            this.txtMashApeKey.Name = "txtMashApeKey";
            this.txtMashApeKey.Size = new System.Drawing.Size(488, 20);
            this.txtMashApeKey.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 94);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(81, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Refresh Token:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 68);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Access Token:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "MashApe Key";
            // 
            // txtImgurAccountId
            // 
            this.txtImgurAccountId.Location = new System.Drawing.Point(403, 117);
            this.txtImgurAccountId.Name = "txtImgurAccountId";
            this.txtImgurAccountId.Size = new System.Drawing.Size(185, 20);
            this.txtImgurAccountId.TabIndex = 1;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(309, 120);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Account Id:";
            // 
            // txtImgurAccountUsername
            // 
            this.txtImgurAccountUsername.Location = new System.Drawing.Point(100, 117);
            this.txtImgurAccountUsername.Name = "txtImgurAccountUsername";
            this.txtImgurAccountUsername.Size = new System.Drawing.Size(185, 20);
            this.txtImgurAccountUsername.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 120);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Account Username:";
            // 
            // txtImgurClientId
            // 
            this.txtImgurClientId.Location = new System.Drawing.Point(100, 13);
            this.txtImgurClientId.Name = "txtImgurClientId";
            this.txtImgurClientId.Size = new System.Drawing.Size(185, 20);
            this.txtImgurClientId.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Client ID";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnDwnReddit);
            this.tabPage1.Controls.Add(this.lstDwnArchivos);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtDwnUrl);
            this.tabPage1.Controls.Add(this.prbDwnProgreso);
            this.tabPage1.Controls.Add(this.chkIterar);
            this.tabPage1.Controls.Add(this.txtDwnIteracionesDesde);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.lblAvance);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.btnDwnImagenes);
            this.tabPage1.Controls.Add(this.txtDwnNombreAlbum);
            this.tabPage1.Controls.Add(this.txtDwnIteracionesCant);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(606, 515);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Descargar Imagenes";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnDwnReddit
            // 
            this.btnDwnReddit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDwnReddit.Location = new System.Drawing.Point(6, 95);
            this.btnDwnReddit.Name = "btnDwnReddit";
            this.btnDwnReddit.Size = new System.Drawing.Size(82, 33);
            this.btnDwnReddit.TabIndex = 14;
            this.btnDwnReddit.Text = "Reddit Likes";
            this.btnDwnReddit.UseVisualStyleBackColor = true;
            this.btnDwnReddit.Click += new System.EventHandler(this.btnDwnReddit_Click);
            // 
            // lstDwnArchivos
            // 
            this.lstDwnArchivos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstDwnArchivos.FullRowSelect = true;
            this.lstDwnArchivos.Location = new System.Drawing.Point(6, 170);
            this.lstDwnArchivos.MultiSelect = false;
            this.lstDwnArchivos.Name = "lstDwnArchivos";
            this.lstDwnArchivos.Size = new System.Drawing.Size(594, 339);
            this.lstDwnArchivos.TabIndex = 13;
            this.lstDwnArchivos.UseCompatibleStateImageBehavior = false;
            this.lstDwnArchivos.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Origen";
            this.columnHeader1.Width = 300;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Destino";
            this.columnHeader2.Width = 290;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.txtImgDescripcion);
            this.tabPage2.Controls.Add(this.lstImgArchivos);
            this.tabPage2.Controls.Add(this.prbImgProgreso);
            this.tabPage2.Controls.Add(this.btnImgIr);
            this.tabPage2.Controls.Add(this.btnImgSeleccionarCarpeta);
            this.tabPage2.Controls.Add(this.txtImgUrl);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txtImgCarpetaOrigen);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtImgNombreAlbum);
            this.tabPage2.Controls.Add(this.btnImgSubirUser);
            this.tabPage2.Controls.Add(this.btnImgSubir);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(606, 515);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Imgur Upload";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 90);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 13);
            this.label23.TabIndex = 16;
            this.label23.Text = "Descripcion";
            // 
            // txtImgDescripcion
            // 
            this.txtImgDescripcion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImgDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImgDescripcion.Location = new System.Drawing.Point(6, 106);
            this.txtImgDescripcion.Name = "txtImgDescripcion";
            this.txtImgDescripcion.Size = new System.Drawing.Size(594, 29);
            this.txtImgDescripcion.TabIndex = 15;
            // 
            // lstImgArchivos
            // 
            this.lstImgArchivos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.lstImgArchivos.FullRowSelect = true;
            this.lstImgArchivos.Location = new System.Drawing.Point(6, 229);
            this.lstImgArchivos.MultiSelect = false;
            this.lstImgArchivos.Name = "lstImgArchivos";
            this.lstImgArchivos.Size = new System.Drawing.Size(594, 280);
            this.lstImgArchivos.TabIndex = 14;
            this.lstImgArchivos.UseCompatibleStateImageBehavior = false;
            this.lstImgArchivos.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Origen";
            this.columnHeader3.Width = 300;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Destino";
            this.columnHeader4.Width = 290;
            // 
            // prbImgProgreso
            // 
            this.prbImgProgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prbImgProgreso.Location = new System.Drawing.Point(9, 180);
            this.prbImgProgreso.Name = "prbImgProgreso";
            this.prbImgProgreso.Size = new System.Drawing.Size(594, 43);
            this.prbImgProgreso.TabIndex = 11;
            // 
            // btnImgSeleccionarCarpeta
            // 
            this.btnImgSeleccionarCarpeta.Location = new System.Drawing.Point(556, 17);
            this.btnImgSeleccionarCarpeta.Name = "btnImgSeleccionarCarpeta";
            this.btnImgSeleccionarCarpeta.Size = new System.Drawing.Size(44, 23);
            this.btnImgSeleccionarCarpeta.TabIndex = 4;
            this.btnImgSeleccionarCarpeta.TabStop = false;
            this.btnImgSeleccionarCarpeta.Text = "...";
            this.btnImgSeleccionarCarpeta.UseVisualStyleBackColor = true;
            this.btnImgSeleccionarCarpeta.Click += new System.EventHandler(this.btnImgSeleccionarCarpeta_Click);
            // 
            // txtImgCarpetaOrigen
            // 
            this.txtImgCarpetaOrigen.BackColor = System.Drawing.SystemColors.Info;
            this.txtImgCarpetaOrigen.Location = new System.Drawing.Point(6, 19);
            this.txtImgCarpetaOrigen.Name = "txtImgCarpetaOrigen";
            this.txtImgCarpetaOrigen.ReadOnly = true;
            this.txtImgCarpetaOrigen.Size = new System.Drawing.Size(544, 20);
            this.txtImgCarpetaOrigen.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Carpeta de origen:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Nombre del album en Imgur";
            // 
            // txtImgNombreAlbum
            // 
            this.txtImgNombreAlbum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImgNombreAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImgNombreAlbum.Location = new System.Drawing.Point(6, 58);
            this.txtImgNombreAlbum.Name = "txtImgNombreAlbum";
            this.txtImgNombreAlbum.Size = new System.Drawing.Size(594, 29);
            this.txtImgNombreAlbum.TabIndex = 6;
            // 
            // btnImgSubirUser
            // 
            this.btnImgSubirUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImgSubirUser.Location = new System.Drawing.Point(464, 141);
            this.btnImgSubirUser.Name = "btnImgSubirUser";
            this.btnImgSubirUser.Size = new System.Drawing.Size(136, 33);
            this.btnImgSubirUser.TabIndex = 10;
            this.btnImgSubirUser.TabStop = false;
            this.btnImgSubirUser.Text = "Subir Imagenes";
            this.btnImgSubirUser.UseVisualStyleBackColor = true;
            this.btnImgSubirUser.Click += new System.EventHandler(this.btnImgSubirUser_Click);
            // 
            // btnImgSubir
            // 
            this.btnImgSubir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImgSubir.Location = new System.Drawing.Point(316, 141);
            this.btnImgSubir.Name = "btnImgSubir";
            this.btnImgSubir.Size = new System.Drawing.Size(142, 33);
            this.btnImgSubir.TabIndex = 10;
            this.btnImgSubir.TabStop = false;
            this.btnImgSubir.Text = "Subir Imagenes (Anonimo)";
            this.btnImgSubir.UseVisualStyleBackColor = true;
            this.btnImgSubir.Click += new System.EventHandler(this.btnImgSubir_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lstTmbArchivos);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.txtTmbCaption);
            this.tabPage3.Controls.Add(this.prbTmbProgreso);
            this.tabPage3.Controls.Add(this.btnTmbSeleccionarCarpeta);
            this.tabPage3.Controls.Add(this.txtTmbCarpetaOrigen);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.btnTmbSubir);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(606, 515);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tumblr Upload";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lstTmbArchivos
            // 
            this.lstTmbArchivos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTmbArchivos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lstTmbArchivos.FullRowSelect = true;
            this.lstTmbArchivos.Location = new System.Drawing.Point(6, 181);
            this.lstTmbArchivos.MultiSelect = false;
            this.lstTmbArchivos.Name = "lstTmbArchivos";
            this.lstTmbArchivos.Size = new System.Drawing.Size(594, 328);
            this.lstTmbArchivos.TabIndex = 24;
            this.lstTmbArchivos.UseCompatibleStateImageBehavior = false;
            this.lstTmbArchivos.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Imagenes";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Destino";
            this.columnHeader6.Width = 400;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Status";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Titulo para las imagenes";
            // 
            // txtTmbCaption
            // 
            this.txtTmbCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTmbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTmbCaption.Location = new System.Drawing.Point(6, 58);
            this.txtTmbCaption.Name = "txtTmbCaption";
            this.txtTmbCaption.Size = new System.Drawing.Size(594, 29);
            this.txtTmbCaption.TabIndex = 23;
            // 
            // prbTmbProgreso
            // 
            this.prbTmbProgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prbTmbProgreso.Location = new System.Drawing.Point(6, 132);
            this.prbTmbProgreso.Name = "prbTmbProgreso";
            this.prbTmbProgreso.Size = new System.Drawing.Size(594, 43);
            this.prbTmbProgreso.TabIndex = 21;
            // 
            // btnTmbSeleccionarCarpeta
            // 
            this.btnTmbSeleccionarCarpeta.Location = new System.Drawing.Point(556, 17);
            this.btnTmbSeleccionarCarpeta.Name = "btnTmbSeleccionarCarpeta";
            this.btnTmbSeleccionarCarpeta.Size = new System.Drawing.Size(44, 23);
            this.btnTmbSeleccionarCarpeta.TabIndex = 14;
            this.btnTmbSeleccionarCarpeta.TabStop = false;
            this.btnTmbSeleccionarCarpeta.Text = "...";
            this.btnTmbSeleccionarCarpeta.UseVisualStyleBackColor = true;
            this.btnTmbSeleccionarCarpeta.Click += new System.EventHandler(this.btnTmbSeleccionarCarpeta_Click);
            // 
            // txtTmbCarpetaOrigen
            // 
            this.txtTmbCarpetaOrigen.BackColor = System.Drawing.SystemColors.Info;
            this.txtTmbCarpetaOrigen.Location = new System.Drawing.Point(6, 19);
            this.txtTmbCarpetaOrigen.Name = "txtTmbCarpetaOrigen";
            this.txtTmbCarpetaOrigen.ReadOnly = true;
            this.txtTmbCarpetaOrigen.Size = new System.Drawing.Size(544, 20);
            this.txtTmbCarpetaOrigen.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Carpeta de origen:";
            // 
            // btnTmbSubir
            // 
            this.btnTmbSubir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTmbSubir.Location = new System.Drawing.Point(437, 93);
            this.btnTmbSubir.Name = "btnTmbSubir";
            this.btnTmbSubir.Size = new System.Drawing.Size(163, 33);
            this.btnTmbSubir.TabIndex = 20;
            this.btnTmbSubir.TabStop = false;
            this.btnTmbSubir.Text = "Subir Imagenes";
            this.btnTmbSubir.UseVisualStyleBackColor = true;
            this.btnTmbSubir.Click += new System.EventHandler(this.btnTmbSubir_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.lstTelArchivos);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.txtTelNroDestino);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.txtTelCaption);
            this.tabPage4.Controls.Add(this.btnTelEnviar);
            this.tabPage4.Controls.Add(this.prbTelProgreso);
            this.tabPage4.Controls.Add(this.btnTelSeleccionarCarpeta);
            this.tabPage4.Controls.Add(this.txtTelCarpetaOrigen);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(606, 515);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Telegram Upload";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // lstTelArchivos
            // 
            this.lstTelArchivos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTelArchivos.FullRowSelect = true;
            this.lstTelArchivos.Location = new System.Drawing.Point(6, 190);
            this.lstTelArchivos.MultiSelect = false;
            this.lstTelArchivos.Name = "lstTelArchivos";
            this.lstTelArchivos.Size = new System.Drawing.Size(594, 319);
            this.lstTelArchivos.TabIndex = 28;
            this.lstTelArchivos.UseCompatibleStateImageBehavior = false;
            this.lstTelArchivos.View = System.Windows.Forms.View.Details;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 90);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Nro Destino:";
            // 
            // txtTelNroDestino
            // 
            this.txtTelNroDestino.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTelNroDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelNroDestino.Location = new System.Drawing.Point(6, 106);
            this.txtTelNroDestino.Name = "txtTelNroDestino";
            this.txtTelNroDestino.Size = new System.Drawing.Size(163, 29);
            this.txtTelNroDestino.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Mensaje para las imagenes";
            // 
            // txtTelCaption
            // 
            this.txtTelCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTelCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelCaption.Location = new System.Drawing.Point(6, 58);
            this.txtTelCaption.Name = "txtTelCaption";
            this.txtTelCaption.Size = new System.Drawing.Size(594, 29);
            this.txtTelCaption.TabIndex = 25;
            // 
            // btnTelEnviar
            // 
            this.btnTelEnviar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTelEnviar.Location = new System.Drawing.Point(437, 102);
            this.btnTelEnviar.Name = "btnTelEnviar";
            this.btnTelEnviar.Size = new System.Drawing.Size(163, 33);
            this.btnTelEnviar.TabIndex = 23;
            this.btnTelEnviar.TabStop = false;
            this.btnTelEnviar.Text = "Enviar Imagenes";
            this.btnTelEnviar.UseVisualStyleBackColor = true;
            this.btnTelEnviar.Click += new System.EventHandler(this.btnTelEnviar_Click);
            // 
            // prbTelProgreso
            // 
            this.prbTelProgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prbTelProgreso.Location = new System.Drawing.Point(6, 141);
            this.prbTelProgreso.Name = "prbTelProgreso";
            this.prbTelProgreso.Size = new System.Drawing.Size(594, 43);
            this.prbTelProgreso.TabIndex = 22;
            // 
            // btnTelSeleccionarCarpeta
            // 
            this.btnTelSeleccionarCarpeta.Location = new System.Drawing.Point(556, 17);
            this.btnTelSeleccionarCarpeta.Name = "btnTelSeleccionarCarpeta";
            this.btnTelSeleccionarCarpeta.Size = new System.Drawing.Size(44, 23);
            this.btnTelSeleccionarCarpeta.TabIndex = 17;
            this.btnTelSeleccionarCarpeta.TabStop = false;
            this.btnTelSeleccionarCarpeta.Text = "...";
            this.btnTelSeleccionarCarpeta.UseVisualStyleBackColor = true;
            this.btnTelSeleccionarCarpeta.Click += new System.EventHandler(this.btnTelSeleccionarCarpeta_Click);
            // 
            // txtTelCarpetaOrigen
            // 
            this.txtTelCarpetaOrigen.BackColor = System.Drawing.SystemColors.Info;
            this.txtTelCarpetaOrigen.Location = new System.Drawing.Point(6, 19);
            this.txtTelCarpetaOrigen.Name = "txtTelCarpetaOrigen";
            this.txtTelCarpetaOrigen.ReadOnly = true;
            this.txtTelCarpetaOrigen.Size = new System.Drawing.Size(544, 20);
            this.txtTelCarpetaOrigen.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Carpeta de origen:";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.txtRedditPassword);
            this.groupBox4.Controls.Add(this.txtRedditUsername);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Location = new System.Drawing.Point(6, 401);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(591, 47);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Reddit";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(3, 19);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Usuario:";
            // 
            // txtRedditUsername
            // 
            this.txtRedditUsername.Location = new System.Drawing.Point(97, 16);
            this.txtRedditUsername.Name = "txtRedditUsername";
            this.txtRedditUsername.Size = new System.Drawing.Size(185, 20);
            this.txtRedditUsername.TabIndex = 1;
            // 
            // txtRedditPassword
            // 
            this.txtRedditPassword.Location = new System.Drawing.Point(372, 16);
            this.txtRedditPassword.Name = "txtRedditPassword";
            this.txtRedditPassword.PasswordChar = '*';
            this.txtRedditPassword.Size = new System.Drawing.Size(213, 20);
            this.txtRedditPassword.TabIndex = 2;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(288, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Password:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 561);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReAl WebImages ";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtDwnIteracionesCant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDwnIteracionesDesde)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPage0.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtDwnUrl;
        private Button btnDwnImagenes;
        private Label label1;
        internal ProgressBar prbDwnProgreso;
        internal Label lblAvance;
        private Label label4;
        private Label label3;
        internal TextBox txtImgUrl;
        internal TextBox txtDwnNombreAlbum;
        private Button btnImgIr;
        public CheckBox chkIterar;
        private NumericUpDown txtDwnIteracionesCant;
        private NumericUpDown txtDwnIteracionesDesde;
        private Label label5;
        private Label label6;
        private TabControl tabControl;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private Label label7;
        private TextBox txtImgNombreAlbum;
        private Button btnImgSubir;
        internal TextBox txtImgCarpetaOrigen;
        private Label label8;
        private Button btnImgSeleccionarCarpeta;
        internal ProgressBar prbImgProgreso;
        private TabPage tabPage3;
        internal ProgressBar prbTmbProgreso;
        private Button btnTmbSeleccionarCarpeta;
        internal TextBox txtTmbCarpetaOrigen;
        private Label label9;
        private Button btnTmbSubir;
        private Label label2;
        private TextBox txtTmbCaption;
        private TabPage tabPage4;
        private Button btnTelSeleccionarCarpeta;
        internal TextBox txtTelCarpetaOrigen;
        private Label label10;
        internal ProgressBar prbTelProgreso;
        private Button btnTelEnviar;
        private Label label11;
        private TextBox txtTelCaption;
        private Label label12;
        private TextBox txtTelNroDestino;
        private TabPage tabPage0;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private TextBox txtTumblrBlog;
        private Label label19;
        private TextBox txtTumblrTokenSecret;
        private Label label18;
        private TextBox txtTumblrToken;
        private Label label17;
        private TextBox txtTumblrConsumerSecret;
        private Label label16;
        private TextBox txtTumblrConsumerKey;
        private Label label15;
        private TextBox txtMashApeKey;
        private Label label14;
        private TextBox txtImgurClientId;
        private Label label13;
        private GroupBox groupBox3;
        private TextBox txtTelegramNumero;
        private Label label22;
        private TextBox txtTelegramApiHash;
        private Label label21;
        private TextBox txtTelegramApiId;
        private Label label20;
        private Button btnConfigLoad;
        private Button btnConfigSave;
        private ListView lstDwnArchivos;
        private ListView lstImgArchivos;
        private ListView lstTmbArchivos;
        private ListView lstTelArchivos;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader3;
        private ColumnHeader columnHeader4;
        private ColumnHeader columnHeader5;
        private ColumnHeader columnHeader6;
        private ColumnHeader columnHeader7;
        private Button btnTelegramAutorizar;
        private Button btnConfApply;
        private Button btnImgSubirUser;
        private Label label23;
        private TextBox txtImgDescripcion;
        private TextBox txtImgurAccessToken;
        private Label label24;
        private TextBox txtImgurRefreshToken;
        private Label label25;
        private TextBox txtImgurAccountId;
        private Label label27;
        private TextBox txtImgurAccountUsername;
        private Label label26;
        private Button btnImgurTokens;
        private Button btnDwnReddit;
        private GroupBox groupBox4;
        private TextBox txtRedditUsername;
        private Label label28;
        private Label label29;
        private TextBox txtRedditPassword;
    }
}

