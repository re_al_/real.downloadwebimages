﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using RedditSharp;
using RedditSharp.Things;

namespace ReAl.DownloadWebImages.App.SocialHelpers
{
    public static class CRedditHelper
    {
        private static Reddit reddit;
        private static bool authenticated = false;

        static CRedditHelper()
        {
            try
            {
                reddit = new Reddit(CSettings.strRedditUsername, CSettings.strRedditPassword);
                authenticated = (reddit.User != null);
            }
            catch (AuthenticationException exp)
            {
                Console.WriteLine("Incorrect login.");
                Console.WriteLine(exp.Message);
                authenticated = false;
            }
        }

        public static ArrayList GetLikedPosts()
        {
            ArrayList arrImages = new ArrayList();
            if (authenticated)
            {
                List<Post> misPost = reddit.User.LikedPosts.ToList();
                foreach (Post post in misPost)
                {
                    Console.WriteLine(post.Url.ToString());
                    arrImages.Add(post.Url.ToString() + "|" + post.LinkFlairText +"-" + post.Title);
                }
            }

            return arrImages;
        }
    }
}
