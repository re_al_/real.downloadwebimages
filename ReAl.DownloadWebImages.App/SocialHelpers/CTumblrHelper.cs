﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using DontPanic.TumblrSharp;
using DontPanic.TumblrSharp.Client;
using DontPanic.TumblrSharp.OAuth;

namespace ReAl.DownloadWebImages.App.SocialHelpers
{
    public static class CTumblrHelper
    {
        public static string Blog { get; set; }
     
        private static TumblrClient client;

        static CTumblrHelper()
        {
            // create our client
            client = new TumblrClientFactory().Create<TumblrClient>(CSettings.strTumblrConsumerKey, CSettings.strTumblrConsumerSecret, new Token(CSettings.strTumblrToken, CSettings.strTumblrTokenSecret));
        }

        public static string uploadImageToTumblr(string strImagePath, string strCaption = "")
        {
            var myBite = File.ReadAllBytes(strImagePath);
            BinaryFile data = new BinaryFile(myBite);
            Task<PostCreationInfo> post = client.CreatePostAsync(CSettings.strTumblrBlog, PostData.CreatePhoto(data, strCaption));
            PostCreationInfo result = post.Result;
            return "Status:" + post.Status + ": PostId" + result.PostId.ToString();
        }

        public static string uploadImageToTumblr(ArrayList arrImagePath, string strCaption = "")
        {
            List<BinaryFile> arrBinaryFile = new List<BinaryFile>();
            foreach (string strImagePath in arrImagePath)
            {
                var myBite = File.ReadAllBytes(strImagePath);
                BinaryFile data = new BinaryFile(myBite);
                arrBinaryFile.Add(data);
            }

            Task<PostCreationInfo> post = client.CreatePostAsync(CSettings.strTumblrBlog, PostData.CreatePhoto(arrBinaryFile, strCaption) );            
            Thread.Sleep(500);
            PostCreationInfo result = post.Result;
            return post.Status + "|" + result.PostId.ToString();
        }
    }
}
