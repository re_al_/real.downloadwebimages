﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace ReAl.DownloadWebImages.App.SocialHelpers
{
    internal static class CImgurHelper
    {
        
        public static void createAlbumImgur(string strImgurClientId, string strAlbumName, ref string strAlbumId, ref string strAlbumDeleteHash)
        {
            using (var w = new customWebClient())
            {
                w.Headers.Add("Authorization", "Client-ID " + strImgurClientId);
                if (CSettings.strMashApeKey != "")
                    w.Headers.Add("X-Mashape-Key", CSettings.strMashApeKey);
                var values = new NameValueCollection
                {
                    {"title", "\"" +strAlbumName + "\""},
                    { "account_url", null }
                };
                byte[] response = null;
                if (CSettings.strMashApeKey != "")
                    response = w.UploadValues("https://imgur-apiv3.p.mashape.com/3/album.xml", values);
                else
                    response = w.UploadValues("https://api.imgur.com/3/album.xml", values);
                strAlbumId = XDocument.Load(new MemoryStream(response)).Element("data").Element("id").Value;
                strAlbumDeleteHash = XDocument.Load(new MemoryStream(response)).Element("data").Element("deletehash").Value;
                //strAlbumUrl = XDocument.Load(new MemoryStream(response)).Element("data").Element("link").Value;
            }
            
        }

        public static string uploadImageToImgur(string strImgurClientId, string strAlbumDeleteHash, string strImagePath)
        {
            using (var w = new customWebClient())
            {
                Thread.Sleep(100);
                w.Headers.Add("Authorization", "Client-ID " + strImgurClientId);
                if (CSettings.strMashApeKey != "")
                    w.Headers.Add("X-Mashape-Key", CSettings.strMashApeKey);
                var values = new NameValueCollection
                            {
                                {"image", Convert.ToBase64String(File.ReadAllBytes(strImagePath))},
                                {"album", strAlbumDeleteHash}
                            };
                byte[] response = null;
                if (CSettings.strMashApeKey != "")
                    response = w.UploadValues("https://imgur-apiv3.p.mashape.com/3/upload.xml", values);
                else
                    response = w.UploadValues("https://api.imgur.com/3/upload.xml", values);
                return XDocument.Load(new MemoryStream(response)).Element("data").Element("link").Value;

            }
        }

        public static async Task<ArrayList> getImagesFromAlbum(string strImgurClientId, string strImgurlAlbumId)
        {
            ArrayList arrImgurImageLink = new ArrayList();
            imgurRootObject<imgurAlbum> imgRoot = null;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Client-ID " + strImgurClientId);
                if (CSettings.strMashApeKey != "")
                    client.DefaultRequestHeaders.Add("X-Mashape-Key", CSettings.strMashApeKey);

                HttpResponseMessage response = await client.GetAsync(new Uri("https://imgur-apiv3.p.mashape.com/3/album/" + strImgurlAlbumId));
                string content = await response.Content.ReadAsStringAsync();

                imgRoot = JsonConvert.DeserializeObject<imgurRootObject<imgurAlbum>>(content);
            }

            foreach (imgurImage image in imgRoot.Data.Images)
            {
                arrImgurImageLink.Add(image.Link);
            }
            return arrImgurImageLink;
        }
    }

    public class imgurRootObject<T>
    {
        [JsonProperty("data")]
        public T Data { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    public class imgurAlbum
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public object Description { get; set; }

        [JsonProperty("datetime")]
        public int Datetime { get; set; }

        [JsonProperty("cover")]
        public string Cover { get; set; }

        [JsonProperty("account_url")]
        public string AccountUrl { get; set; }

        [JsonProperty("privacy")]
        public string Privacy { get; set; }

        [JsonProperty("layout")]
        public string Layout { get; set; }

        [JsonProperty("views")]
        public int Views { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("images_count")]
        public int ImagesCount { get; set; }

        [JsonProperty("images")]
        public List<imgurImage> Images { get; set; }
    }

    public class imgurImage
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("datetime")]
        public int Datetime { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("animated")]
        public bool Animated { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("views")]
        public long Views { get; set; }

        [JsonProperty("bandwidth")]
        public long Bandwidth { get; set; }

        [JsonProperty("deletehash")]
        public string Deletehash { get; set; }

        [JsonProperty("section")]
        public object Section { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }
    }
}