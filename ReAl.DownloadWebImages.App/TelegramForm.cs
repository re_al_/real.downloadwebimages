﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReAl.DownloadWebImages.App
{
    public partial class TelegramForm : Form
    {
        private string strCodigoRecibido = "";
        public TelegramForm()
        {
            InitializeComponent();
            strCodigoRecibido = "";
        }

        public string CodigoRecibido()
        {
            return strCodigoRecibido;
        }

        private void btnTelegramAutorizar_Click(object sender, EventArgs e)
        {
            this.strCodigoRecibido = txtTelegramCodigo.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
