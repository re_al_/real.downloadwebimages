﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using HtmlAgilityPack;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Enums;
using Imgur.API.Models.Impl;
using ReAl.DownloadWebImages.App.SocialHelpers;
using TeleSharp.TL;
using TLSharp.Core;
using TLSharp.Core.Utils;

namespace ReAl.DownloadWebImages.App
{
    public partial class Form1 : Form
    {
        //KB: http://stackoverflow.com/questions/27289454/download-all-images-from-webpage-using-c-sharp

        
        //Para descargar
        private Boolean _complete = false;       
        private WebBrowser browser = new WebBrowser();        
        private int iIntentos = 1;
        private int iIntentosComplete = 1;
        private string strConfigFileName = "Integrate.WebImages.cif";
        private int iAvance = 1;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
        }

        private void enableGui(bool bMensaje, long miliseconds)
        {
            txtDwnUrl.Text = "";
            txtDwnNombreAlbum.Enabled = true;
            btnDwnImagenes.Enabled = true;
            btnDwnReddit.Enabled = true;
            txtDwnIteracionesDesde.Enabled = true;
            txtDwnIteracionesCant.Enabled = true;
            prbDwnProgreso.Value = 0;
            lblAvance.Text = "Listos?? Fuera!!";
            if (bMensaje)
            {
                DialogResult res = MessageBox.Show(this,
                    "Se han descargado las imagenes en "+ miliseconds+" milisegundos ¿Desea abrir la carpeta contenedora?", "Exito!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information);
                if (res == DialogResult.Yes)
                {
                    string strDirecotryApp = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                        "\\DownloadWebImages";
                    Process.Start("explorer.exe", strDirecotryApp);
                }
            }
        }
        
        public ArrayList getImagesFromUrl(ArrayList urls)
        {
            WebBrowser webBrowser1 = new WebBrowser();
            ArrayList arrImageUrls = new ArrayList();

            foreach (string strUrl in urls)
            {
                ArrayList arrUrl = populateArray(ref webBrowser1, strUrl);
                arrImageUrls.AddRange(arrUrl);
            }
            return arrImageUrls;
        }

        private ArrayList populateArray(ref WebBrowser webBrowser1, string strUrl)
        {
            webBrowser1.ScriptErrorsSuppressed = true;
            webBrowser1.Navigate(strUrl);

            waitTillLoad(webBrowser1);

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            var documentAsIHtmlDocument3 = (mshtml.IHTMLDocument3)webBrowser1.Document.DomDocument;
            StringReader sr = new StringReader(documentAsIHtmlDocument3.documentElement.outerHTML);
            doc.Load(sr);

            ArrayList arrImageUrls = new ArrayList();

            List<HtmlNode> imageNodes = null;
            imageNodes = (from HtmlNode node in doc.DocumentNode.SelectNodes("//img")
                          where node.Name == "img"
                          select node).ToList();

            foreach (HtmlNode node in imageNodes)
            {
                arrImageUrls.Add(node.Attributes["src"].Value);
            }

            List<HtmlNode> imageNodesA = null;
            imageNodesA = (from HtmlNode node in doc.DocumentNode.SelectNodes("//a")
                           where node.Name == "a"
                           select node).ToList();

            foreach (HtmlNode node in imageNodesA)
            {
                if (node.Attributes["href"] != null)
                    arrImageUrls.Add(node.Attributes["href"].Value);
            }

            ArrayList arrImagesDistinct = new ArrayList();
            foreach (string aString in arrImageUrls)
            {
                if (!arrImagesDistinct.Contains(aString))
                    arrImagesDistinct.Add(aString);
            }

            return arrImagesDistinct;
        }

        private void waitTillLoad(WebBrowser webBrControl)
        {
            WebBrowserReadyState loadStatus;
            int waittime = 300;
            int counter = 0;
            while (true)
            {
                Thread.Sleep(1000);
                loadStatus = webBrControl.ReadyState;
                Application.DoEvents();
                if ((counter > waittime) || (loadStatus == WebBrowserReadyState.Uninitialized) || (loadStatus == WebBrowserReadyState.Loading) || (loadStatus == WebBrowserReadyState.Interactive))
                {
                    break;
                }
                counter++;
            }

            counter = 0;
            while (true)
            {
                Thread.Sleep(1000);
                loadStatus = webBrControl.ReadyState;
                Application.DoEvents();
                if (loadStatus == WebBrowserReadyState.Complete && webBrControl.IsBusy != true)
                {
                    break;
                }
                counter++;
            }
        }

        public void downloadImages(ArrayList arrOriginal, string strDirectory)
        {
            lblAvance.Text = "Se han obtenido " + arrOriginal.Count + " direcciones a descargar...";
            lblAvance.Refresh();
            prbDwnProgreso.Minimum = 0;
            prbDwnProgreso.Maximum = arrOriginal.Count;

            int iHilos = 4;
            List<ArrayList> arrHilos = new List<ArrayList>();

            //iniciamos los arrays
            for (int i = 0; i < iHilos; i++)
                arrHilos.Add(new ArrayList());

            //Llenamos los valores
            for (int i = 0; i < arrOriginal.Count; i++)
                (arrHilos[i % iHilos]).Add(arrOriginal[i]);

            lblAvance.Text = "Descargando " + arrOriginal.Count + " archivos en " + iHilos + " hilos...";
            lblAvance.Refresh();

            var watch = Stopwatch.StartNew();

            //Lista de Tasks
            Task[] misTareas = new Task[4];
            int iContador = 0;
            foreach (ArrayList arrHilo in arrHilos)
            {
                Task<int> miTarea = DonwloadImagesAsync(arrHilo, strDirectory);
                misTareas[iContador] = miTarea;
                iContador++;
            }

            Task.Factory.ContinueWhenAll(misTareas, (tasks) =>
            {
                foreach (var task in tasks)
                {
                    if (task.Status == TaskStatus.RanToCompletion)
                    {
                        Console.WriteLine(task.Id);
                    }
                    else
                    {
                        Console.WriteLine("An error has occurred..");
                        Console.WriteLine(task.Exception.InnerException.Message);
                    }
                    Console.WriteLine();
                    Console.WriteLine("------------------------------------------------");
                    Console.WriteLine();
                }

                watch.Stop();
                enableGui(true, watch.ElapsedMilliseconds);
            });
        }

        private string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_").Replace(";","").Replace("(","").Replace(")","");
        }

        private Task<int> DonwloadImagesAsync(ArrayList arrUrls, string strDirectory)
        {
            try
            {
                return Task<int>.Factory.StartNew(() =>
                {
                    using (WebClient client = new WebClient())
                    {
                        foreach (string url in arrUrls)
                        {
                            string strUrl = url;
                            strUrl = strUrl.Split('|')[0];

                            //Vemos el nombre
                            string strNombre = strUrl;
                            if (url.Split('|').Length > 0)
                            {
                                strNombre = url.Split('|')[1];
                            }
                            else
                            {
                                strNombre = new string(url.Skip(url.LastIndexOf('/') + 1).ToArray());
                                strNombre =
                                    HttpUtility.UrlDecode(strNombre)
                                        .ToString()
                                        .Replace("%28", "(")
                                        .Replace("%29", ")")
                                        .Replace("%", "");


                            }

                            if (Path.GetExtension(strDirectory + "\\" + strNombre) == "")
                                strNombre = strNombre + ".png";
                            var strDestino = strDirectory + "\\" + MakeValidFileName(strNombre);


                            try
                            {
                                File.WriteAllBytes(strDestino, client.DownloadData(strUrl));

                                prbDwnProgreso.Value = prbDwnProgreso.Value + 1;

                                Size miTamaño = cImageHelper.GetDimensions(strDestino);

                                if (!strNombre.ToUpper().EndsWith("GIF"))
                                {
                                    if (miTamaño.Width < 800 || miTamaño.Height < 600)
                                    {
                                        //File.Delete(strDirectory + "\\" + filename);
                                        //Movemos la imagen a una subcarpeta
                                        if (!Directory.Exists(strDirectory + "\\peq\\"))
                                            Directory.CreateDirectory(strDirectory + "\\peq\\");
                                        {
                                            File.Move(strDestino,
                                                strDirectory + "\\peq\\" + strNombre);
                                            strDestino = strDirectory + "\\peq\\" + strNombre;
                                        }
                                    }

                                    //Añadimos el registro al detalle
                                    ListViewItem lvi = new ListViewItem(strUrl);
                                    lvi.SubItems.Add(strDestino);
                                    lstDwnArchivos.Items.Add(lvi);
                                    lstDwnArchivos.Refresh();
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);                                
                            }                            
                        }
                    }
                    return arrUrls.Count;
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        private void btnDwnImagenes_Click(object sender, EventArgs e)
        {
            iAvance = 0;
            string strCarpetaAlbum = "";
            btnDwnImagenes.Enabled = false;
            btnDwnReddit.Enabled = false;
            txtDwnNombreAlbum.Enabled = false;
            txtDwnIteracionesDesde.Enabled = false;
            txtDwnIteracionesCant.Enabled = false;
            lblAvance.Text = "Preparando todo";
            lblAvance.Refresh();

            //Para las carpetas
            strCarpetaAlbum = txtDwnNombreAlbum.Text.Trim() == ""
                ? "ReAl_" + DateTime.Now.ToString("yyyyMMdd_HHmmss")
                : txtDwnNombreAlbum.Text.Trim();
            string strDirecotryApp = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                        "\\DownloadWebImages";
            if (!Directory.Exists(strDirecotryApp))
                Directory.CreateDirectory(strDirecotryApp);

            string strDirectory = strDirecotryApp + "\\" + strCarpetaAlbum;
            if (!Directory.Exists(strDirectory))
                Directory.CreateDirectory(strDirectory);

            //Revisamos la URL
            ArrayList arrImages = new ArrayList();
            if (txtDwnUrl.Text.ToUpper().Contains("IMGUR"))
            {                
                string strAlbumId = new string(txtDwnUrl.Text.Skip(txtDwnUrl.Text.LastIndexOf('/') + 1).ToArray()); ;
                Task<ArrayList> tarea = CImgurHelper.getImagesFromAlbum(CSettings.strImgurClientId, strAlbumId);
                arrImages = tarea.Result;
                downloadImages(arrImages, strDirectory);
            }
            else
            {
                ArrayList arrUrls = new ArrayList();
                if (chkIterar.Checked)
                {
                    for (decimal i = txtDwnIteracionesDesde.Value; i <= txtDwnIteracionesCant.Value; i++)
                        arrUrls.Add(txtDwnUrl.Text.Replace("%%", i.ToString()));
                    arrImages = getImagesFromUrl(arrUrls);
                }
                else
                {
                    arrUrls.Add(txtDwnUrl.Text);
                    arrImages = getImagesFromUrl(arrUrls);
                }

                //Descragamos las imagenes
                downloadImages(arrImages, strDirectory);
            }
        }

        private void btnIr_Click(object sender, EventArgs e)
        {
            if (txtImgUrl.Text.Trim() != "")
                Process.Start(txtImgUrl.Text);
        }

        private DateTime getImageDate(string strUrl)
        {
            try
            {
                Uri myUri = new Uri(strUrl);

                // Creates an HttpWebRequest for the specified URL.
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(myUri);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                    Console.WriteLine(
                        "\r\nRequest succeeded and the requested information is in the response , Description : {0}",
                        myHttpWebResponse.StatusDescription);

                DateTime today = DateTime.Now;

                // Uses the LastModified property to compare with today's date.
                if (DateTime.Compare(today, myHttpWebResponse.LastModified) == 0)
                    return today;
                else
                {
                    if (DateTime.Compare(today, myHttpWebResponse.LastModified) == 1)
                        return myHttpWebResponse.LastModified;

                    // Releases the resources of the response.
                    myHttpWebResponse.Close();
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                Console.WriteLine(exp.StackTrace);
            }
            return DateTime.Now;
        }

        private void chkIterar_CheckedChanged(object sender, EventArgs e)
        {
            txtDwnIteracionesCant.Enabled = chkIterar.Checked;
            txtDwnIteracionesDesde.Enabled = chkIterar.Checked;
        }

        private void btnImgSubir_Click(object sender, EventArgs e)
        {
            try
            {
                lstImgArchivos.Items.Clear();
                lstImgArchivos.Refresh();

                //Preparamos los datos
                string strImgurAlbum = txtImgNombreAlbum.Text.Trim();
                string strAlbumId = "";
                string strAlbumDeleteHash = "";

                //Creamos el album en Imgur
                CImgurHelper.createAlbumImgur(CSettings.strImgurClientId, strImgurAlbum, ref strAlbumId, ref strAlbumDeleteHash);
                txtImgUrl.Text = "http://imgur.com/a/" + strAlbumId + "?grid";
                txtImgUrl.Refresh();

                string[] files = Directory.GetFiles(txtImgCarpetaOrigen.Text);

                prbImgProgreso.Minimum = 0;
                prbImgProgreso.Maximum = files.Length;
                int iAvance = 0;

                string strResultado = "";
                foreach (string file in files)
                {
                    prbImgProgreso.Value = iAvance;
                    if (file.ToUpper().EndsWith(".JPG") || file.ToUpper().EndsWith(".JPEG") ||
                        file.ToUpper().EndsWith(".PNG"))
                    {
                        try
                        {
                            strResultado = CImgurHelper.uploadImageToImgur(CSettings.strImgurClientId, strAlbumDeleteHash, file);
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception);
                            strResultado = exception.Message + (exception.InnerException == null
                                               ? ""
                                               : "\r\n" + exception.InnerException.Message);
                        }
                        //Añadimos el registro al detalle
                        ListViewItem lvi = new ListViewItem(file);
                        lvi.SubItems.Add(strResultado);
                        lstImgArchivos.Items.Add(lvi);
                        lstImgArchivos.Refresh();

                        //mostramos el ultimo
                        var items = lstImgArchivos.Items;
                        var last = items[items.Count - 1];
                        last.EnsureVisible();
                    }
                    iAvance++;
                }

                prbImgProgreso.Value = 0;


                DialogResult res = MessageBox.Show(this,
                        "Se han subido las imagenes a Imgur", "Exito!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                prbImgProgreso.Value = 0;
                Console.WriteLine(exception);
                MessageBox.Show(this,
                    exception.Message + "\r\n" + (exception.InnerException == null
                        ? ""
                        : exception.InnerException.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnImgSeleccionarCarpeta_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtImgCarpetaOrigen.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnTmbSeleccionarCarpeta_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtTmbCarpetaOrigen.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnTmbSubir_Click(object sender, EventArgs e)
        {
            try
            {
                lstTmbArchivos.Items.Clear();
                lstTmbArchivos.Refresh();
                string[] files = Directory.GetFiles(txtTmbCarpetaOrigen.Text);

                prbTmbProgreso.Minimum = 0;
                prbTmbProgreso.Maximum = files.Length;
                int iAvance = 0;
                

                //Bloques de 10 en 10
                int iContador = 1;
                ArrayList arrFotos = new ArrayList();
                string strResultado = "";

                foreach (string file in files)
                {
                    
                    prbTmbProgreso.Value = iAvance;
                    if (file.ToUpper().EndsWith(".JPG") || file.ToUpper().EndsWith(".JPEG") ||
                        file.ToUpper().EndsWith(".PNG"))
                    {
                        //agregamos al array
                        arrFotos.Add(file);
                        if (iContador < 5)
                            iContador++;
                        else
                        {
                            try
                            {
                                //publicamos
                                strResultado = CTumblrHelper.uploadImageToTumblr(arrFotos, txtTmbCaption.Text);
                            }
                            catch (Exception exception)
                            {
                                strResultado = exception.Message;
                            }
                                                            
                            
                            //Añadimos el registro al detalle
                            ListViewItem lvi = new ListViewItem(arrFotos.Count + " imagenes");                            
                            lvi.SubItems.Add(CSettings.strTumblrBlog + "/posts/" + strResultado.Split('|')[1]);
                            lvi.SubItems.Add(strResultado.Split('|')[0]);
                            lstTmbArchivos.Items.Add(lvi);
                            lstTmbArchivos.Refresh();

                            iContador = 1;
                            arrFotos = new ArrayList();
                        }                       
                    }
                    iAvance++;
                }

                //Subimos los restantes
                if (arrFotos.Count > 0)
                {
                    strResultado = CTumblrHelper.uploadImageToTumblr(arrFotos, txtTmbCaption.Text);

                    //Añadimos el registro al detalle
                    ListViewItem lvi = new ListViewItem(arrFotos.Count + " img");
                    lvi.SubItems.Add(CSettings.strTumblrBlog + "/post/" + strResultado.Split('|')[1]);
                    lvi.SubItems.Add(strResultado.Split('|')[0]);
                    lstTmbArchivos.Items.Add(lvi);
                    lstTmbArchivos.Refresh();
                }

                prbTmbProgreso.Value = 0;


                DialogResult res = MessageBox.Show(this,
                        "Se han subido las imagenes a Tumblr", "Exito!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                prbTmbProgreso.Value = 0;
                Console.WriteLine(exception);
                MessageBox.Show(this,
                    exception.Message + "\r\n" + (exception.InnerException == null
                        ? ""
                        : exception.InnerException.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTelSeleccionarCarpeta_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtTelCarpetaOrigen.Text = fbd.SelectedPath;
                }
            }
        }

        private async void btnTelEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                //Telegram                
                var store = new FileSessionStore();
                var client = new TelegramClient(CSettings.intTelegramApiId, CSettings.strTelegramApiHash, store, "re_al_");
                await client.ConnectAsync();
                if (true)
                {
                    var estaRegistrado = await client.IsPhoneRegisteredAsync(CSettings.strTelegramNumero);
                    if (estaRegistrado)
                    {
                        if (client.IsUserAuthorized())
                        {
                            var result = await client.GetContactsAsync();
                            var destino = result.Users.ToList()
                                .OfType<TLUser>()
                                .FirstOrDefault(x => x.Phone == txtTelNroDestino.Text);

                            if (destino != null)
                            {
                                string[] files = Directory.GetFiles(txtTelCarpetaOrigen.Text);

                                prbTelProgreso.Minimum = 0;
                                prbTelProgreso.Maximum = files.Length;
                                int iAvance = 0;

                                foreach (string file in files)
                                {
                                    prbTelProgreso.Value = iAvance;
                                    if (file.ToUpper().EndsWith(".JPG") || file.ToUpper().EndsWith(".JPEG") ||
                                        file.ToUpper().EndsWith(".PNG"))
                                    {
                                        if (iAvance % 5 == 0)
                                            Thread.Sleep(20 * 1000);

                                        //Telegram
                                        FileInfo info = new FileInfo(file);
                                        var archivo = await client.UploadFile(info.Name + "." + info.Extension, new StreamReader(file));
                                        await client.SendUploadedPhoto(new TLInputPeerUser() { UserId = destino.Id }, archivo, txtTelCaption.Text);
                                    }
                                    iAvance++;
                                }
                                prbTelProgreso.Value = 0;

                                DialogResult res = MessageBox.Show(this,
                                        "Se han enviado las imagenes a Telegram", "Exito!",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                            }                           
                        }
                        else
                        {
                            throw new Exception("Esta aplicacion no ha sido autorizada para el Numero " + CSettings.strTelegramNumero);
                        }
                    }
                }               
            }
            catch (Exception exception)
            {
                prbTelProgreso.Value = 0;
                Console.WriteLine(exception);
                MessageBox.Show(this,
                    exception.Message + "\r\n" + (exception.InnerException == null
                        ? ""
                        : exception.InnerException.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfigLoad_Click(object sender, EventArgs e)
        {
            try
            {

                if (File.Exists(strConfigFileName))
                {
                    CargarConfiguracion();
                    AplicarConfiguracion();
                }
                else
                    MessageBox.Show(this, "No existe el archivo de configuración", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
            catch (Exception exception)
            {
                prbTelProgreso.Value = 0;
                Console.WriteLine(exception);
                MessageBox.Show(this,
                    exception.Message + "\r\n" + (exception.InnerException == null
                        ? ""
                        : exception.InnerException.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfigSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Guardamos la Conexion
                DataTable dt = new DataTable("Configuracion");
                DataColumn dc1 = new DataColumn("Campo", typeof(String));
                DataColumn dc2 = new DataColumn("Valor", typeof(String));

                dt.Columns.Add(dc1);
                dt.Columns.Add(dc2);

                //Eliminamos la configuracion si existe
                if (File.Exists(strConfigFileName))
                    File.Delete(strConfigFileName);

                //añadimos los valores
                DataRow dr = dt.NewRow();
                dr[0] = "strTumblrConsumerKey";
                dr[1] = txtTumblrConsumerKey.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strTumblrConsumerSecret";
                dr[1] = txtTumblrConsumerSecret.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strTumblrToken";
                dr[1] = txtTumblrToken.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strTumblrTokenSecret";
                dr[1] = txtTumblrTokenSecret.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strTumblrBlog";
                dr[1] = txtTumblrBlog.Text;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr[0] = "strImgurClientId";
                dr[1] = txtImgurClientId.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strMashApeKey";
                dr[1] = txtMashApeKey.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strImgurAccessToken";
                dr[1] = txtImgurAccessToken.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strImgurRefreshToken";
                dr[1] = txtImgurRefreshToken.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strImgurAccountUsername";
                dr[1] = txtImgurAccountUsername.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strImgurAccountId";
                dr[1] = txtImgurAccountId.Text;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr[0] = "intTelegramApiId";
                dr[1] = txtTelegramApiId.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strTelegramApiHash";
                dr[1] = txtTelegramApiHash.Text;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr[0] = "strTelegramNumero";
                dr[1] = txtTelegramNumero.Text;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr[0] = "strRedditUsername";
                dr[1] = txtRedditUsername.Text;
                dt.Rows.Add(dr);

                dt.WriteXml(strConfigFileName.Replace(".cif",""));

                
                UTF8Encoding myEncoding = new UTF8Encoding();
                Byte[] myLlave = myEncoding.GetBytes(CSettings.strLlaveEncriptacion);
                CConfigHelper.cifrarXor(strConfigFileName.Replace(".cif", ""), myLlave);

                if (File.Exists(strConfigFileName.Replace(".cif", "")))
                    File.Delete(strConfigFileName.Replace(".cif", ""));

                MessageBox.Show(
                    "Se ha creado satisfactoriamente el archivo de configuracion con la información introducida",
                    "Exito!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                prbTelProgreso.Value = 0;
                Console.WriteLine(exception);
                MessageBox.Show(this,
                    exception.Message + "\r\n" + (exception.InnerException == null
                        ? ""
                        : exception.InnerException.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CargarConfiguracion()
        {
            //DesEncriptamos            
            UTF8Encoding myEncoding = new UTF8Encoding();
            Byte[] myLlave = myEncoding.GetBytes(CSettings.strLlaveEncriptacion);

            if (File.Exists(strConfigFileName + ".decif"))
                File.Delete(strConfigFileName + ".decif");

            CConfigHelper.descifrarXor(strConfigFileName, myLlave);

            if (File.Exists(strConfigFileName + ".decif"))
            {
                //Cargamos el DataTable
                DataTable dt = new DataTable("Configuracion");
                DataColumn dc1 = new DataColumn("Campo", typeof(String));
                DataColumn dc2 = new DataColumn("Valor", typeof(String));

                dt.Columns.Add(dc1);
                dt.Columns.Add(dc2);
                dt.ReadXml(strConfigFileName + ".decif");

                //Eliminamos el Archivo
                if (File.Exists(strConfigFileName + ".decif")) File.Delete(strConfigFileName + ".decif");

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i][0].ToString() == "strTumblrConsumerKey") CSettings.strTumblrConsumerKey = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strTumblrConsumerSecret") CSettings.strTumblrConsumerSecret = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strTumblrToken") CSettings.strTumblrToken = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strTumblrTokenSecret") CSettings.strTumblrTokenSecret = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strTumblrBlog") CSettings.strTumblrBlog = dt.Rows[i][1].ToString();

                        if (dt.Rows[i][0].ToString() == "strImgurClientId") CSettings.strImgurClientId = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strMashApeKey") CSettings.strMashApeKey = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strImgurAccessToken") CSettings.strImgurAccessToken = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strImgurRefreshToken") CSettings.strImgurRefreshToken = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strImgurAccountUsername") CSettings.strImgurAccountUsername = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strImgurAccountId") CSettings.strImgurAccountId = dt.Rows[i][1].ToString();

                        if (dt.Rows[i][0].ToString() == "intTelegramApiId") CSettings.intTelegramApiId = int.Parse(dt.Rows[i][1].ToString());
                        if (dt.Rows[i][0].ToString() == "strTelegramApiHash") CSettings.strTelegramApiHash = dt.Rows[i][1].ToString();
                        if (dt.Rows[i][0].ToString() == "strTelegramNumero") CSettings.strTelegramNumero = dt.Rows[i][1].ToString();

                        if (dt.Rows[i][0].ToString() == "strRedditUsername") CSettings.strRedditUsername = dt.Rows[i][1].ToString();
                    }

                    //Mostramos la informacion en el formulario:
                    txtTumblrConsumerKey.Text = CSettings.strTumblrConsumerKey;
                    txtTumblrConsumerSecret.Text = CSettings.strTumblrConsumerSecret;
                    txtTumblrToken.Text = CSettings.strTumblrToken;
                    txtTumblrTokenSecret.Text = CSettings.strTumblrTokenSecret;
                    txtTumblrBlog.Text = CSettings.strTumblrBlog;

                    txtImgurClientId.Text = CSettings.strImgurClientId;
                    txtMashApeKey.Text = CSettings.strMashApeKey;
                    txtImgurAccessToken.Text = CSettings.strImgurAccessToken;
                    txtImgurRefreshToken.Text = CSettings.strImgurRefreshToken;
                    txtImgurAccountUsername.Text = CSettings.strImgurAccountUsername;
                    txtImgurAccountId.Text = CSettings.strImgurAccountId;

                    txtTelegramApiId.Text = CSettings.intTelegramApiId.ToString();
                    txtTelegramApiHash.Text = CSettings.strTelegramApiHash;
                    txtTelegramNumero.Text = CSettings.strTelegramNumero;

                    txtRedditUsername.Text = CSettings.strRedditUsername;
                }
            }
            else
            {
                MessageBox.Show(
                    "No se ha podido DesEncriptar el archivo de Configuración",
                    "Error al leer el Archivo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private async void btnTelegramAutorizar_Click(object sender, EventArgs e)
        {
            try
            {
                //Telegram                
                var store = new FileSessionStore();
                var client = new TelegramClient(CSettings.intTelegramApiId, CSettings.strTelegramApiHash, store, "re_al_");
                await client.ConnectAsync();
                if (true)
                {
                    var estaRegistrado = await client.IsPhoneRegisteredAsync(CSettings.strTelegramNumero);
                    if (estaRegistrado)
                    {
                        if (client.IsUserAuthorized())
                        {
                            MessageBox.Show("La Aplicacion esta Autorizada", "Exito", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                        }
                        else
                        {
                            string hash = await client.SendCodeRequestAsync(CSettings.strTelegramNumero);

                            var frm = new TelegramForm();
                            DialogResult res = frm.ShowDialog(this);

                            if (res == DialogResult.OK)
                            {
                                var strCodigo = frm.GetCodigoRecibido();

                                var user = await client.MakeAuthAsync(CSettings.strTelegramNumero, hash, strCodigo);

                                MessageBox.Show("La Aplicacion esta Autorizada", "Exito", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                            }
                            else
                                throw new Exception("Debe especificar un codigo de autorizacion valido para Telegram");
                        }
                    }
                    else
                        throw new Exception("El Numero " + CSettings.strTelegramNumero + " no esta registrado en TELEGRAM");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    exception.Message,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void btnConfApply_Click(object sender, EventArgs e)
        {
            AplicarConfiguracion();
        }

        private void AplicarConfiguracion()
        {
            //Tumblr: https://www.tumblr.com/oauth/apps
            CSettings.strTumblrConsumerKey = txtTumblrConsumerKey.Text;
            CSettings.strTumblrConsumerSecret = txtTumblrConsumerSecret.Text;
            CSettings.strTumblrToken = txtTumblrToken.Text;
            CSettings.strTumblrTokenSecret = txtTumblrTokenSecret.Text;
            CSettings.strTumblrBlog = txtTumblrBlog.Text;

            //Imgur: https://imgur.com/account/settings/apps
            CSettings.strImgurClientId = txtImgurClientId.Text;
            CSettings.strMashApeKey = txtMashApeKey.Text;
            CSettings.strImgurAccessToken = txtMashApeKey.Text;
            CSettings.strImgurRefreshToken = txtMashApeKey.Text;
            CSettings.strImgurAccountUsername = txtMashApeKey.Text;
            CSettings.strImgurAccountId = txtMashApeKey.Text;

            //Telegram
            CSettings.intTelegramApiId = int.Parse(txtTelegramApiId.Text);
            CSettings.strTelegramApiHash = txtTelegramApiHash.Text;
            CSettings.strTelegramNumero = txtTelegramNumero.Text;

            //Reddit
            CSettings.strRedditUsername = txtRedditUsername.Text;
        }

        private async void btnImgSubirUser_Click(object sender, EventArgs e)
        {
            try
            {
                var token = new OAuth2Token(CSettings.strImgurAccessToken,
                    CSettings.strImgurRefreshToken, "bearer", CSettings.strImgurAccountId, 
                    CSettings.strImgurAccountUsername, 2419200);
                var client = new MashapeClient(CSettings.strImgurClientId, CSettings.strMashApeKey, token);

                var albumEndpoint = new AlbumEndpoint(client);
                var album = await albumEndpoint.CreateAlbumAsync(txtImgNombreAlbum.Text, txtImgDescripcion.Text, AlbumPrivacy.Public,AlbumLayout.Grid);

                txtImgUrl.Text = "http://imgur.com/a/" + album.Id + "?grid";
                txtImgUrl.Refresh();

                var imageEndpoint = new ImageEndpoint(client);

                string[] files = Directory.GetFiles(txtImgCarpetaOrigen.Text);

                prbImgProgreso.Minimum = 0;
                prbImgProgreso.Maximum = files.Length;
                var iAvance = 0;

                foreach (string file in files)
                {
                    prbImgProgreso.Value = iAvance;
                    if (file.ToUpper().EndsWith(".JPG") || file.ToUpper().EndsWith(".JPEG") ||
                        file.ToUpper().EndsWith(".PNG"))
                    {
                        var strResultado = "";
                        try
                        {
                            var fileUpload = File.ReadAllBytes(file);
                            var image = await imageEndpoint.UploadImageBinaryAsync(fileUpload, album.Id, txtImgNombreAlbum.Text);
                            strResultado = "http://imgur.com/" + image.Id;
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception);
                            strResultado = exception.Message + (exception.InnerException == null
                                               ? ""
                                               : "\r\n" + exception.InnerException.Message);
                        }
                        //Añadimos el registro al detalle
                        ListViewItem lvi = new ListViewItem(file);
                        lvi.SubItems.Add(strResultado);
                        lstImgArchivos.Items.Add(lvi);
                        lstImgArchivos.Refresh();

                        //mostramos el ultimo
                        var items = lstImgArchivos.Items;
                        var last = items[items.Count - 1];
                        last.EnsureVisible();
                    }
                    iAvance++;
                }

                prbImgProgreso.Value = 0;


                DialogResult res = MessageBox.Show(this,
                        "Se han subido las imagenes a Imgur", "Exito!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                prbImgProgreso.Value = 0;
                Console.WriteLine(exception);
                MessageBox.Show(this,
                    exception.Message + "\r\n" + (exception.InnerException == null
                        ? ""
                        : exception.InnerException.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnImgurTokens_Click(object sender, EventArgs e)
        {
            try
            {
                var client = new MashapeClient(CSettings.strImgurClientId, CSettings.strMashApeKey);
                var endpoint = new OAuth2Endpoint(client);
                var authorizationUrl = endpoint.GetAuthorizationUrl(OAuth2ResponseType.Token);
                Process.Start(authorizationUrl);
            }
            catch (Exception exception)
            {
                MessageBox.Show(this,
                    exception.Message + "\r\n" + (exception.InnerException == null
                        ? ""
                        : exception.InnerException.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDwnReddit_Click(object sender, EventArgs e)
        {
            if (txtRedditPassword.Text == "")
            {
                MessageBox.Show(this,
                    "Debe especificar el Password para el usuario de Reddit: " + CSettings.strRedditUsername, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            CSettings.strRedditPassword = txtRedditPassword.Text;

            iAvance = 0;
            string strCarpetaAlbum = "";
            btnDwnImagenes.Enabled = false;
            btnDwnReddit.Enabled = false;
            txtDwnNombreAlbum.Enabled = false;
            txtDwnIteracionesDesde.Enabled = false;
            txtDwnIteracionesCant.Enabled = false;
            lblAvance.Text = "Preparando todo";
            lblAvance.Refresh();

            //Para las carpetas
            strCarpetaAlbum = txtDwnNombreAlbum.Text.Trim() == ""
                ? "ReAl_" + DateTime.Now.ToString("yyyyMMdd_HHmmss")
                : txtDwnNombreAlbum.Text.Trim();
            string strDirecotryApp = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                        "\\DownloadWebImages";
            if (!Directory.Exists(strDirecotryApp))
                Directory.CreateDirectory(strDirecotryApp);

            string strDirectory = strDirecotryApp + "\\" + strCarpetaAlbum;
            if (!Directory.Exists(strDirectory))
                Directory.CreateDirectory(strDirectory);

            //Revisamos la URL
            ArrayList arrImages = CRedditHelper.GetLikedPosts();

            lblAvance.Text = "Se han obtenido " + arrImages.Count + " imagenes...";
            lblAvance.Refresh();
            prbDwnProgreso.Minimum = 0;
            prbDwnProgreso.Maximum = arrImages.Count;

            int iHilos = 4;
            List<ArrayList> arrHilos = new List<ArrayList>();

            //iniciamos los arrays
            for (int i = 0; i < iHilos; i++)
                arrHilos.Add(new ArrayList());

            //Llenamos los valores
            for (int i = 0; i < arrImages.Count; i++)
                (arrHilos[i % iHilos]).Add(arrImages[i]);

            lblAvance.Text = "Descargando " + arrImages.Count + " archivos en " + iHilos + " hilos...";
            lblAvance.Refresh();

            var watch = Stopwatch.StartNew();

            Task[] misTareas = new Task[4];
            int iContador = 0;
            foreach (ArrayList arrHilo in arrHilos)
            {
                Task<int> miTarea = DonwloadImagesAsync(arrHilo, strDirectory);
                misTareas[iContador] = miTarea;
                iContador++;
            }

            Task.Factory.ContinueWhenAll(misTareas, (tasks) =>
            {
                foreach (var task in tasks)
                {
                    if (task.Status == TaskStatus.RanToCompletion)
                    {
                        Console.WriteLine(task.Id);
                    }
                    else
                    {
                        Console.WriteLine("An error has occurred..");
                        Console.WriteLine(task.Exception.InnerException.Message);
                    }
                    Console.WriteLine();
                    Console.WriteLine("------------------------------------------------");
                    Console.WriteLine();
                }

                watch.Stop();
                enableGui(true, watch.ElapsedMilliseconds);
            });
        }
    }
}