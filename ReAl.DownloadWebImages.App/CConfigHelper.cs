﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReAl.DownloadWebImages.App
{
    public class CConfigHelper
    {
        /// <summary>
        ///     Función que crea un archivo encriptado a partir de un path dado y máscara de Bytes
        /// </summary>
        /// <param name="path" type="string">
        ///     <para>
        ///         Dirección física del archivo
        ///     </para>
        /// </param>
        /// <param name="mask" type="byte[]">
        ///     <para>
        ///         Máscara de Bytes
        ///     </para>
        /// </param>
        public static void cifrarXor(string path, byte[] mask)
        {
            FileInfo file = new FileInfo(path);
            file.CopyTo(path + ".cif");
            BufferedStream bsr = new BufferedStream(File.OpenRead(path));
            BufferedStream bsw = new BufferedStream(File.OpenWrite(path + ".cif"));

            while (true)
            {
                int read = bsr.ReadByte();

                if (read < 0)
                {
                    break;
                }
                byte b = (byte)read;

                for (int i = 0; i < mask.Length; i++)
                {
                    b = (byte)(b ^ mask[i]);
                }
                bsw.WriteByte(b);
            }
            bsr.Close();
            bsw.Close();
        }

        /// <summary>
        ///     Función que crea un descifra un archivo encriptado a partir de un path dado y máscara de Bytes
        /// </summary>
        /// <param name="path" type="string">
        ///     <para>
        ///         Dirección física del archivo
        ///     </para>
        /// </param>
        /// <param name="mask" type="byte[]">
        ///     <para>
        ///         Máscara de Bytes
        ///     </para>
        /// </param>
        public static void descifrarXor(string path, byte[] mask)
        {
            FileInfo file = new FileInfo(path);
            file.CopyTo(path + ".decif");
            BufferedStream bsr = new BufferedStream(File.OpenRead(path));
            BufferedStream bsw = new BufferedStream(File.OpenWrite(path + ".decif"));

            while (true)
            {
                int read = bsr.ReadByte();

                if (read < 0)
                {
                    break;
                }
                byte b = (byte)read;

                for (int i = mask.Length - 1; i >= 0; i--)
                {
                    b = (byte)(b ^ mask[i]);
                }
                bsw.WriteByte(b);
            }
            bsr.Close();
            bsw.Close();
        }
    }
}
