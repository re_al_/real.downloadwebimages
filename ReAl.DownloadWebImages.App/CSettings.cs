﻿namespace ReAl.DownloadWebImages.App
{
    public static class CSettings
    {
        //Llave de encriptacion
        internal const string strLlaveEncriptacion = "ReAl.WebImages";

        //Tumblr: https://www.tumblr.com/oauth/apps
        internal static string strTumblrConsumerKey = "";
        internal static string strTumblrConsumerSecret = "";
        internal static string strTumblrToken = "";
        internal static string strTumblrTokenSecret = "";
        internal static string strTumblrBlog = "";

        //Imgur: https://imgur.com/account/settings/apps
        internal static string strImgurClientId = "";
        internal static string strMashApeKey = "";
        internal static string strImgurAccessToken = "";
        internal static string strImgurRefreshToken = "";
        internal static string strImgurAccountUsername = "";
        internal static string strImgurAccountId = "";
        

        //Telegram
        internal static int intTelegramApiId = 0;
        internal static string strTelegramApiHash = "";
        internal static string strTelegramNumero = "";

        //Reddit
        internal static string strRedditUsername = "";
        internal static string strRedditPassword = "";
    }
}
