
# ReAl WebImages 
-----------------

[![Build status](https://ci.appveyor.com/api/projects/status/gf7j1h4lg7iljil1?svg=true)](https://ci.appveyor.com/project/re_al_/real-downloadwebimages)[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=re_al__real.downloadwebimages&metric=alert_status)](https://sonarcloud.io/dashboard?id=re_al__real.downloadwebimages)

## Descarga las imagenes de BLOGSPOT, TUMBLR y demas!!

Aplicacion de Escritorio para descargar Imagenes desde un Sitio Web. 
Las imagenes se almacenaran en el Escritorio en una carpeta con la fecha y hora
Adicionalmente, si se proporciona el ClientID de IMGUR, se subiran las imagenes a un album automaticamente

Ahora se pueden descargar imagenes de varias paginas de TUMBlR expresadasa en un rango.
Por ejemplo tenemos la siguiente URL de TUMBLR:
https://miblog.tumblr.com/tagged/etiqueta/page/4
Si queremos obtener las imagenes desde la pagina 5 a la 7, se debera colocar la siguiente URL:
https://miblog.tumblr.com/tagged/etiqueta/page/%%
Y marcar la opcion "Iterar paginas %%", especificando los valores 5 y 7 en laos campos "Desde" y "Al".

En caso de iterar blogs y paginas, los archivos recibiran una notacion Page##_NombreArchivo en la carpeta de destino

KB: http://stackoverflow.com/questions/27289454/download-all-images-from-webpage-using-c-sharp

## Sube imagenes a IMGUR

La Aplicacion permite subir las imagenes de una carpeta a IMGUR. Para ello se crea un album ANONIMO y se asocian todas las imagenes.
Al final la aplicación muestra la URL del album para poder ver las imagenes


## Sube imagenes a TUMBLR

La Aplicacion permite subir las imagenes de una carpeta a TUMBLR. Se crea un post cada CINCO imagenes de la carpeta seleccionada


## Envia imagenes a TELEGRAM

La aplicacion permite el envio de imagenes a un numero de telefono en especifico.
Se pedira un codigo de autorizacion para acceder a Telegram.


## Futuras caracteristicas

  - Filtrar las imagenes por fecha 

  - Filtrar las imagenes por TAGs
  

## Instalador de la aplicacion para Windows:

  - [ReAl.DownloadWebImages.x86]  
  
  - [ReAl.DownloadWebImages.x64]  
  
  
[ReAl.DownloadWebImages.x86]: https://bitbucket.org/re_al_/real.downloadwebimages/raw/master/ReAl.DownloadWebImages.x86/Debug/ReAl.DownloadWebImages.x86.msi

[ReAl.DownloadWebImages.x64]:https://bitbucket.org/re_al_/real.downloadwebimages/raw/master/ReAl.DownloadWebImages.x64/Debug/ReAl.DownloadWebImages.x64.msi
  
[![buddy pipeline](https://app.buddy.works/re_al/real-downloadwebimages/pipelines/pipeline/40082/badge.svg?token=1409015e0f8a609b4f2981e62d60b77f86a96f33bc78bcb5c60b3031d6f402e3 "buddy pipeline")](https://app.buddy.works/re_al/real-downloadwebimages/pipelines/pipeline/40082)